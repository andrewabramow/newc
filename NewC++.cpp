/*
�� �������� ������������ ����� �� 1 �� 5,
��������� auto � initializer_list.
*/

#include <iostream>
#include <vector>

int main()
{
    std::vector<int> num{ 1,2,3,4,5 };

    ////for (int i = 1; i < 6; ++i) {
    ////    std::cout << i << " ";
    ////}

    for (auto& i : num) {
        std::cout << i << " ";
    }
}

